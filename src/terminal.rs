use std::cmp;
use std::io::{self, Write};
use termion::event::Event;
use termion::input::{Events, MouseTerminal, TermRead};
use termion::raw::{IntoRawMode, RawTerminal};
use termion::terminal_size;
use termion::{clear, color, cursor, style};
use unicode_segmentation::UnicodeSegmentation;

use crate::buffer::Buffer;
use crate::editor::UI;
use crate::lang::Language;
use crate::line::Line;
use crate::position::Position;
use crate::style::{stylize, Style};

const STATUS_FG_COLOR: color::Rgb = color::Rgb(255, 255, 248);
const STATUS_BG_COLOR: color::Rgb = color::Rgb(17, 17, 17);

pub struct Terminal {
    stdout: MouseTerminal<RawTerminal<io::Stdout>>,
    width: u16,
    height: u16,
    offset: Position,
    events: Events<io::Stdin>,
}

impl Terminal {
    pub fn new() -> io::Result<Self> {
        let stdin = io::stdin();

        let stdout = MouseTerminal::from(io::stdout().into_raw_mode()?);

        let size = terminal_size()?;
        Ok(Terminal {
            stdout,
            events: stdin.events(),
            width: size.0,
            // Make room for a status bar
            height: size.1.saturating_sub(2),
            offset: Position::default(),
        })
    }

    pub fn draw_cursor(&self, buf: &mut Vec<u8>, cursor: &Position) -> io::Result<()> {
        let Position { mut x, mut y } = cursor.clone();

        x = x.saturating_add(1).saturating_sub(self.offset.x);
        y = y.saturating_add(1).saturating_sub(self.offset.y);

        buf.write_fmt(format_args!("{}", cursor::Goto(x as u16, y as u16)))?;
        //buf.extend_from_slice(cursor::BlinkingBlock.as_ref());
        buf.extend_from_slice(cursor::Show.as_ref());

        Ok(())
    }

    pub fn draw_document(&self, buf: &mut Vec<u8>, bfr: &Buffer) {
        let lang = bfr.language();
        for termrow in 0..self.height {
            buf.extend_from_slice(clear::CurrentLine.as_ref());
            let offset = termrow as usize + self.offset.y;
            if let Some(row) = bfr.document.line(offset) {
                self.draw_row(buf, row, lang);
            } else {
                buf.extend_from_slice(b"~\r\n");
            }
        }
    }

    pub fn draw_status_bar(&self, buf: &mut Vec<u8>, bfr: &Buffer) {
        let width = self.width as usize;

        let file_name = if let Some(mut fname) = bfr.document.file_name.clone() {
            fname.truncate(20);
            fname
        } else {
            "[No Name]".to_string()
        };
        let Position { x, y } = bfr.cursor;
        let mut status = String::with_capacity(width);
        status.push(' ');
        status.push_str(&file_name);
        let line_indicator = format!("{}:{} ", y.saturating_add(1), x);
        // Pad status to right-align the line indicator
        let len = status.len() + line_indicator.len();
        status.push_str(&" ".repeat(width.saturating_sub(len)));

        status.push_str(&line_indicator);
        status.truncate(width);

        buf.extend_from_slice(STATUS_BG_COLOR.bg_string().as_bytes());
        buf.extend_from_slice(STATUS_FG_COLOR.fg_string().as_bytes());

        buf.extend_from_slice(status.as_bytes());
        buf.extend_from_slice(b"\r\n");

        buf.extend_from_slice(color::Reset.bg_str().as_bytes());
        buf.extend_from_slice(color::Reset.fg_str().as_bytes());
    }

    pub fn draw_message_bar(&self, buf: &mut Vec<u8>, bfr: &Buffer) {
        buf.extend_from_slice(clear::CurrentLine.as_ref());
        if let Some(status) = &bfr.status {
            if !status.needs_display() {
                return;
            }
            let message = status.as_ref();
            let len = cmp::min(message.len(), self.width as usize);
            buf.extend_from_slice(message[..len].as_bytes());
        }
    }

    pub fn draw_row(&self, buf: &mut Vec<u8>, row: &Line, lang: &Language) {
        let start = self.offset.x;
        let end = self.offset.x + self.width as usize;
        //
        let end = cmp::min(end, row.len());
        let start = cmp::min(start, end);

        let mut iter = row
            .as_ref()
            .grapheme_indices(true)
            .skip(start)
            .take(end - start);
        let styles = stylize(lang, row.as_ref());
        let mut spans = styles.iter();
        let mut current_style = spans.next();
        // TODO stylize document
        while let Some((idx, g)) = iter.next() {
            if let Some(span) = current_style {
                if idx == span.start {
                    // if idx == start, insert start seq
                    // I dislike this
                    let bs = match span.style {
                        Style::Bold => (style::Bold {}).as_ref(),
                        Style::Faint => (style::Faint {}).as_ref(),
                        Style::Italic => (style::Italic {}).as_ref(),
                        Style::Underline => (style::Underline {}).as_ref(),
                        Style::CrossedOut => (style::CrossedOut {}).as_ref(),
                        Style::Invert => (style::Invert {}).as_ref(),
                        Style::Framed => (style::Framed {}).as_ref(),
                        _ => (style::Reset {}).as_ref(),
                    };

                    buf.extend_from_slice(bs);
                }
                buf.extend_from_slice(g.as_bytes());
                if idx == span.end {
                    // if idx == end, append end seq
                    buf.extend_from_slice(style::Reset.as_ref());
                    current_style = spans.next();
                }
            } else {
                buf.extend_from_slice(g.as_bytes());
            }
        }

        //
        buf.extend_from_slice(b"\r\n");
    }

    // https://vt100.net/docs/vt100-ug/chapter3.html#ED
    pub fn clear_screen(&mut self) -> io::Result<()> {
        let mut wr = self.stdout.lock();
        wr.write_all(clear::All.as_ref())?;
        wr.write_fmt(format_args!("{}", cursor::Goto(1, 1)))?;
        wr.flush()
    }
}

impl UI for Terminal {
    fn read_input(&mut self) -> io::Result<Event> {
        loop {
            if let Some(ev) = self.events.next() {
                return Ok(ev?);
            }
        }
    }

    fn render(&mut self, bfr: &Buffer, cursor: &Position) -> io::Result<()> {
        let mut wr = self.stdout.lock();
        let mut buf = Vec::with_capacity(bfr.document.size());

        buf.write_fmt(format_args!("{}", cursor::Goto(1, 1)))?;
        buf.extend_from_slice(cursor::Hide.as_ref());

        self.draw_document(&mut buf, bfr);
        self.draw_status_bar(&mut buf, bfr);
        self.draw_message_bar(&mut buf, bfr);
        self.draw_cursor(&mut buf, &cursor)?;

        wr.write_all(&buf[..])?;
        wr.flush()
    }
    fn scroll(&mut self, cursor: &Position) {
        let Position { x, y } = cursor.clone();
        let width = self.width as usize;
        let height = self.height as usize;
        let mut offset = &mut self.offset;
        if y < offset.y {
            offset.y = y;
        } else if y >= offset.y.saturating_add(height) {
            offset.y = y.saturating_sub(height).saturating_add(1);
        }
        if x < offset.x {
            offset.x = x;
        } else if x >= offset.x.saturating_add(width) {
            offset.x = x.saturating_sub(width).saturating_add(1);
        }
    }
    fn bounds(&self) -> (usize, usize) {
        (self.width as usize, self.height as usize)
    }
}
