use crate::style::Span;
use crate::unicode::GraphemeExt;
use std::cmp::PartialEq;
use std::convert::AsRef;
use unicode_segmentation::UnicodeSegmentation;

#[derive(Debug, Default)]
pub struct Line {
    string: String,
    styles: Vec<Span>,
    len: usize,
}

impl PartialEq for Line {
    fn eq(&self, other: &Self) -> bool {
        self.string == other.string
    }
}

impl Line {
    pub fn new(string: String) -> Self {
        let len = string[..].graphemes(true).count();
        Line {
            string,
            len,
            styles: Vec::new(),
        }
    }

    /// Return the indentation level, measured in spaces
    /// TODO handle spaces vs tabs and return it
    pub fn indentation(&self) -> usize {
        self.string[..]
            .chars()
            .take_while(|c| c.is_whitespace())
            .count()
    }

    /// Return the number of unicode graphemes in this Line.
    ///
    /// ```rust
    /// # use kilo::line::Line;
    /// # use std::error::Error;
    /// #
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let s = String::from("❤");
    /// assert!(s.len() == 3);
    /// let l = Line::from(s);
    /// assert!(l.len() == 1);
    /// # Ok(())
    /// # }
    /// ```
    pub fn len(&self) -> usize {
        self.len
    }

    fn update_len(&mut self) {
        self.len = self.string[..].graphemes(true).count();
    }

    /// Insert a character at the specified grapheme index.
    pub fn insert(&mut self, at: usize, ch: char) {
        self.string.insert_grapheme(at, ch);
        self.update_len();
    }

    pub fn delete(&mut self, at: usize) {
        self.string.remove_grapheme(at);
        self.update_len();
    }

    pub fn append(&mut self, other: Line) {
        self.string.push_str(&other.string);
        self.len += other.len;
    }

    pub fn split(&mut self, at: usize) -> Line {
        if at == self.len() {
            return Line::default();
        }
        let remainder = Line::from(self.string.split_grapheme(at));
        self.update_len();
        remainder
    }

    pub fn find(&self, needle: &str, after: usize) -> Option<usize> {
        self.string.find_grapheme(needle, after)
    }

    pub fn rfind(&self, needle: &str, before: usize) -> Option<usize> {
        self.string.rfind_grapheme(needle, before)
    }

    pub fn as_bytes(&self) -> &[u8] {
        self.string.as_bytes()
    }
}

impl AsRef<str> for Line {
    fn as_ref(&self) -> &str {
        self.string.as_ref()
    }
}

impl From<&str> for Line {
    fn from(s: &str) -> Self {
        s.to_owned().into()
    }
}

impl From<String> for Line {
    fn from(string: String) -> Self {
        let len = string.graphemes(true).count();
        Self {
            string,
            len,
            styles: Vec::new(),
        }
    }
}

impl std::fmt::Display for Line {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.string)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use k9::assert_equal;

    #[test]
    fn line_insert_ascii() {
        let mut line = Line::from("helloworld");
        line.insert(5, ' ');
        assert_equal!(line, Line::from("hello world"));
    }

    #[test]
    fn line_insert_unicode() {
        let mut line = Line::from("hello❤❤world");
        line.insert(6, ' ');
        assert_equal!(line, Line::from("hello❤ ❤world"));
    }

    #[test]
    fn line_insert_append() {
        let mut line = Line::from("he");
        line.insert(2, 'y');
        assert_equal!(line, Line::from("hey"));
    }

    #[test]
    fn line_delete_ascii() {
        let mut line = Line::from("hello world");
        line.delete(5);
        assert_equal!(line, Line::from("helloworld"));
    }

    #[test]
    fn line_delete_unicode() {
        let mut line = Line::from("hello❤ ❤world");
        line.delete(6);
        assert_equal!(line, Line::from("hello❤❤world"));
    }

    #[test]
    fn line_split_empty() {
        let mut line = Line::default();
        let new_line = line.split(0);
        assert_equal!(line, Line::default());
        assert_equal!(new_line, Line::default());
    }

    #[test]
    fn line_split() {
        let mut line = Line::from("hello❤ ❤world");
        let new_line = line.split(6);
        assert_equal!(line, Line::from("hello❤"));
        assert_equal!(new_line, Line::from(" ❤world"));
    }
}
