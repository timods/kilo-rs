use crate::document::Document;
use crate::lang::{self, Language};
use crate::line::Line;
use crate::position::Position;
use std::cmp;
use std::convert::{AsRef, From};
use std::path::Path;
use std::time::Instant;

#[derive(Debug)]
pub enum Movement {
    Up,
    Down,
    Left,
    Right,
    Start,
    End,
    ScrollUp(usize),
    ScrollDown(usize),
}

#[derive(Debug)]
pub struct Buffer {
    pub cursor: Position,
    pub document: Document,
    pub language: Language,
    pub status: Option<Status>,
}

impl Buffer {
    pub fn new(document: Document, language: Language) -> Self {
        Buffer {
            cursor: Position::default(),
            document,
            language,
            status: None,
        }
    }

    pub fn open<P: AsRef<Path>>(p: P, language: Language) -> Self {
        let cursor = Position::default();
        match Document::open(p) {
            Ok(document) => Self {
                cursor,
                document,
                language,
                status: None,
            },
            Err(e) => Self {
                cursor,
                document: Document::empty(),
                language: lang::text(),
                status: Some(e.into()),
            },
        }
    }

    pub fn set_cursor(&mut self, pos: Position) {
        self.cursor = pos;
    }

    pub fn move_cursor(&mut self, mv: Movement) {
        use Movement::*;
        let height = self.document.len();
        let Position { mut x, mut y } = self.cursor;
        match mv {
            Up => {
                if y > 0 {
                    y -= 1;
                }
                x = cmp::min(x, self.document.line_len(y));
            }
            Down => {
                if y < height {
                    y += 1;
                }
                x = cmp::min(x, self.document.line_len(y));
            }
            Left => {
                if x > 0 {
                    x -= 1;
                } else if y > 0 {
                    y -= 1;
                    x = self.document.line_len(y);
                }
            }
            Right => {
                let width = self.document.line_len(y);
                if x < width {
                    x += 1;
                } else if y < height {
                    y += 1;
                    x = 0;
                }
            }
            Start => {
                x = 0;
            }
            End => {
                x = self.document.line_len(y);
            }
            ScrollUp(lines) => {
                if y > lines {
                    y -= lines;
                } else {
                    y = 0;
                }
                x = cmp::min(x, self.document.line_len(y));
            }
            ScrollDown(lines) => {
                if lines < (height - y) {
                    y += lines;
                } else {
                    y = height;
                }
                x = cmp::min(x, self.document.line_len(y));
            }
        }
        self.cursor = Position { x, y };
    }

    pub fn insert(&mut self, ch: char) -> usize {
        self.document.insert(&self.cursor, ch)
    }

    pub fn delete(&mut self) {
        self.document.delete(&self.cursor)
    }

    pub fn line(&self, index: usize) -> Option<&Line> {
        self.document.line(index)
    }

    pub fn is_dirty(&self) -> bool {
        self.document.dirty
    }

    pub fn find(&self, needle: &str, after: &Position) -> Option<Position> {
        self.document.find(needle, after)
    }

    pub fn rfind(&self, needle: &str, before: &Position) -> Option<Position> {
        self.document.rfind(needle, before)
    }

    pub fn language(&self) -> &Language {
        &self.language
    }
}

#[derive(Debug)]
pub enum Status {
    Error(String, Instant),
    Prompt(String),
    Message(String, Instant),
}

impl Status {
    pub fn needs_display(&self) -> bool {
        match self {
            Self::Error(_, t) => Instant::now().duration_since(*t).as_secs() < 3,
            Self::Message(_, t) => Instant::now().duration_since(*t).as_secs() < 3,
            Self::Prompt(_) => true,
        }
    }
}

impl<E> From<E> for Status
where
    E: std::error::Error,
{
    fn from(e: E) -> Self {
        Self::Error(format!("{}", e), Instant::now())
    }
}

impl AsRef<str> for Status {
    fn as_ref(&self) -> &str {
        match self {
            Self::Error(msg, _) => msg.as_ref(),
            Self::Prompt(p) => p.as_ref(),
            Self::Message(s, _) => s.as_ref(),
        }
    }
}

impl std::fmt::Display for Status {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Error(msg, _) => write!(f, "{}", msg),
            Self::Prompt(p) => write!(f, "{}: ", p),
            Self::Message(s, _) => write!(f, "{}", s),
        }
    }
}
