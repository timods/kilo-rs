use lazy_static::lazy_static;
use regex::Regex;

use crate::lang::{Annotator, Item};

#[allow(dead_code)]
#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub enum Style {
    Bold,
    Faint,
    Framed,
    Italic,
    Underline,
    CrossedOut,
    Invert,
}

impl From<Item> for Option<Style> {
    fn from(i: Item) -> Self {
        match i {
            Item::Keyword => Some(Style::Bold),
            Item::Type => Some(Style::Italic),
            Item::String => Some(Style::Underline),
            Item::Comment => Some(Style::Faint),
            Item::Literal => None,
            Item::Numeric => None,
            Item::Special => Some(Style::Invert),
        }
    }
}

// TODO use regex?
pub enum Bound<'a> {
    Word,
    Until(&'a str),
    Line,
}

#[derive(Debug, PartialEq, Eq)]
pub struct Span {
    pub start: usize,
    pub end: usize,
    pub style: Style,
}

lazy_static! {
    /// This is an example for using doc comment attributes
    static ref WORD_BREAK: Regex = Regex::new(r"\b").unwrap();
}

/// Stylize a string, returning a vector of spans representing the string to render
/// TODO take a language, not a trait impl, and use a regex set over keywords etc.
pub fn stylize<A: Annotator>(anno: A, s: &str) -> Vec<Span> {
    let mut spans = Vec::new();
    if s.len() == 0 {
        return spans;
    }
    let mut start: usize = 0;
    // TODO use the regex find_iter on keywords
    for mat in WORD_BREAK.find_iter(s) {
        // ID the current style
        let candidate = &s[start..mat.end()];
        if let Some(it) = anno.annotate(candidate) {
            if let Some(st) = it.into() {
                spans.push(Span {
                    start,
                    end: mat.end() - 1,
                    style: st,
                });
            }
        }
        start = mat.end();
    }
    if start < s.len() - 1 {
        // TODO last span
        // ID the current style
        let candidate = &s[start..];
        if let Some(it) = anno.annotate(candidate) {
            if let Some(st) = it.into() {
                spans.push(Span {
                    start,
                    end: s.len() - 1,
                    style: st,
                });
            }
        }
    }
    spans
}

#[cfg(test)]
mod tests {
    use k9::assert_equal;

    struct TestAnno {}

    impl super::Annotator for TestAnno {
        fn stylize(&self, s: &str) -> Option<super::Style> {
            eprintln!("matching against '{}'", s);
            match s {
                "hello" | "no" => Some(super::Style::Bold),
                _ => None,
            }
        }
    }

    #[test]
    fn stylize_none() {
        let s = "hey world";
        assert_equal!(super::stylize(TestAnno {}, s), Vec::new());
    }
    #[test]
    fn stylize_keywords() {
        let s = "hello world no";
        assert_equal!(
            super::stylize(TestAnno {}, s),
            vec![
                super::Span {
                    start: 0,
                    end: 4,
                    style: super::Style::Bold,
                },
                super::Span {
                    start: 12,
                    end: 13,
                    style: super::Style::Bold,
                },
            ]
        );
    }
}
