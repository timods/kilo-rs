use unicode_segmentation::UnicodeSegmentation;

pub trait GraphemeExt {
    /// Insert a unicode grapheme at the specified grapheme, rather than byte, index.
    fn insert_grapheme(&mut self, at: usize, ch: char);
    /// Remove the grapheme at the specified index
    fn remove_grapheme(&mut self, at: usize);
    /// Split a string at the specified grapheme.
    fn split_grapheme(&mut self, at: usize) -> Self;
    /// Find a grapheme after the specified grapheme index
    fn find_grapheme(&self, needle: &str, after: usize) -> Option<usize>;
    /// Reverse find a grapheme before the specified grapheme index
    fn rfind_grapheme(&self, needle: &str, before: usize) -> Option<usize>;
}

// Convert a visual character offset to the actual byte offset it represents
pub fn offset_to_byte_idx(s: &str, offset: usize) -> Option<usize> {
    for (chars, (o, _)) in s.grapheme_indices(true).enumerate() {
        if chars == offset {
            return Some(o);
        }
    }
    None
}

fn byte_idx_to_offset(s: &str, byte_idx: usize) -> Option<usize> {
    for (grapheme_idx, (idx, _)) in s.grapheme_indices(true).enumerate() {
        if idx == byte_idx {
            return Some(grapheme_idx);
        }
    }
    None
}

impl GraphemeExt for String {
    fn insert_grapheme(&mut self, at: usize, ch: char) {
        if let Some(offset) = offset_to_byte_idx(&self, at) {
            self.insert(offset, ch);
        } else {
            self.push(ch);
        }
    }

    fn remove_grapheme(&mut self, at: usize) {
        if let Some(offset) = offset_to_byte_idx(&self, at) {
            // Find the specified grapheme and delete it.
            self.remove(offset);
        }
    }

    fn split_grapheme(&mut self, at: usize) -> String {
        if let Some(offset) = offset_to_byte_idx(&self, at) {
            let new_line = String::from(&self[offset..]);
            self.truncate(offset);
            new_line
        } else {
            String::new()
        }
    }

    fn find_grapheme(&self, needle: &str, after: usize) -> Option<usize> {
        if let Some(offset) = offset_to_byte_idx(&self, after) {
            if let Some(byte_idx) = self[offset..].find(needle) {
                return byte_idx_to_offset(&self, byte_idx + offset);
            }
        }
        None
    }
    fn rfind_grapheme(&self, needle: &str, before: usize) -> Option<usize> {
        if before == self.len() {
            if let Some(byte_idx) = self.rfind(needle) {
                return byte_idx_to_offset(&self, byte_idx);
            }
        }
        if let Some(offset) = offset_to_byte_idx(&self, before) {
            if let Some(byte_idx) = self[..offset].rfind(needle) {
                return byte_idx_to_offset(&self, byte_idx);
            }
        }
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use k9::assert_equal;

    #[test]
    fn insert_ascii() {
        let mut line = String::from("helloworld");
        line.insert_grapheme(5, ' ');
        assert_equal!(line, String::from("hello world"));
    }

    #[test]
    fn insert_unicode() {
        let mut line = String::from("hello❤❤world");
        line.insert_grapheme(6, ' ');
        assert_equal!(line, String::from("hello❤ ❤world"));
    }

    #[test]
    fn insert_append() {
        let mut line = String::from("he");
        line.insert_grapheme(2, 'y');
        assert_equal!(line, String::from("hey"));
    }

    #[test]
    fn remove_ascii() {
        let mut line = String::from("hello world");
        line.remove_grapheme(5);
        assert_equal!(line, String::from("helloworld"));
    }

    #[test]
    fn remove_unicode() {
        let mut line = String::from("hello❤ ❤world");
        line.remove_grapheme(6);
        assert_equal!(line, String::from("hello❤❤world"));
    }

    #[test]
    fn split_empty() {
        let mut line = String::default();
        let new_line = line.split_grapheme(0);
        assert_equal!(line, String::default());
        assert_equal!(new_line, String::default());
    }

    #[test]
    fn split() {
        let mut line = String::from("hello❤ ❤world");
        let new_line = line.split_grapheme(6);
        assert_equal!(line, String::from("hello❤"));
        assert_equal!(new_line, String::from(" ❤world"));
    }

    #[test]
    fn find_ascii() {
        let line = String::from("hello world");
        assert_equal!(line.find_grapheme("o w", 0), Some(4));
    }

    #[test]
    fn find_unicode() {
        let line = String::from("hello wor🧪d");
        assert_equal!(line.find_grapheme("🧪", 0), Some(9));
    }

    #[test]
    fn find_unicode_after() {
        let line = String::from("hello🧪wor🧪d");
        assert_equal!(line.find_grapheme("🧪", 7), Some(9));
    }

    #[test]
    fn rfind_unicode() {
        let line = String::from("hello🧪wor🧪d");
        assert_equal!(line.rfind_grapheme("o", line.len()), Some(7));
    }

    #[test]
    fn rfind_unicode_before() {
        let line = String::from("hello🧪wor🧪d");
        assert_equal!(line.rfind_grapheme("🧪", 7), Some(5));
    }
}
