#![warn(clippy::all, clippy::pedantic, clippy::restriction)]
#![allow(
    clippy::missing_docs_in_private_items,
    clippy::implicit_return,
    clippy::shadow_reuse,
    clippy::print_stdout,
    clippy::wildcard_enum_match_arm,
    clippy::else_if_without_else
)]
use anyhow::Result;
use kilo::document::Document;
use kilo::editor::{Editor, UI};
use kilo::terminal::Terminal;
use std::env;
use std::path::PathBuf;
use std::process;
use structopt::StructOpt;

const VERSION: &str = env!("CARGO_PKG_VERSION");

fn run(f: Option<PathBuf>) -> Result<()> {
    let terminal = match Terminal::new() {
        Ok(t) => t,
        Err(e) => {
            eprintln!("failed to prepare terminal: {:?}", e);
            process::exit(1);
        }
    };

    let (width, height) = terminal.bounds();
    let doc = match f {
        Some(p) => Document::open(p)?,
        None => welcome(width, height),
    };

    let mut editor = Editor::new(terminal, doc);

    Ok(editor.run()?)
}

#[derive(Debug, StructOpt)]
#[structopt(name = "kilo", about, author)]
struct Opt {
    #[structopt(name = "FILE")]
    file: Option<PathBuf>,
}

fn main() {
    let opts = Opt::from_args();
    if let Err(e) = run(opts.file) {
        eprintln!("error: {:?}", e);
        process::exit(1);
    }
}

fn welcome(width: usize, height: usize) -> Document {
    let mut lines = Vec::new();
    for y in 0..height {
        let mut s = String::new();
        if y == height / 3 {
            let msg = format!("Kilo editor -- version {}", VERSION);
            let mut padding = width.saturating_sub(msg.len()) / 2;
            if padding > 0 {
                s.push('~');
                padding -= 1;
            }
            for _ in 0..padding {
                s.push(' ');
            }
            let width = std::cmp::min(width, msg.len());
            if let Some(slice) = msg.get(..width) {
                s.push_str(slice);
            }
        } else {
            s.push('~');
        }
        lines.push(s.into());
    }
    Document::in_memory(lines)
}
