use crate::buffer::{Buffer, Movement, Status};
use crate::document::Document;
use crate::lang;
use crate::position::Position;
use crate::unicode::GraphemeExt;
use std::io;
use std::time::Instant;
use termion::event::{Event, Key, MouseButton, MouseEvent};

const CURSOR_BLOCK: char = '█';

pub trait UI {
    fn bounds(&self) -> (usize, usize);
    fn render(&mut self, bufr: &Buffer, cursor: &Position) -> io::Result<()>;
    fn read_input(&mut self) -> io::Result<Event>;
    fn scroll(&mut self, cursor: &Position);
}

pub struct Editor<U: UI> {
    buffer: Buffer,
    ui: U,
    languages: Vec<lang::Language>,
}

macro_rules! key {
    ( $x:pat ) => {
        Event::Key($x)
    };
}

macro_rules! mouse_press {
    ( $b:pat ) => {
        Event::Mouse(MouseEvent::Press($b, _, _))
    };
}

const HELP: &str = "CTRL-Q to quit, CTRL-W to save";

#[derive(Debug)]
enum Search {
    Forward,
    Backward,
}

impl Search {
    fn apply(&self, buffer: &Buffer, query: &str) -> Option<Position> {
        match self {
            Search::Forward => buffer.find(&query, &buffer.cursor),
            Search::Backward => buffer.rfind(&query, &buffer.cursor),
        }
    }
}

impl<U> Editor<U>
where
    U: UI,
{
    pub fn new(ui: U, doc: Document) -> Self {
        let languages = lang::known();
        let mut l = lang::text();
        if let Some(name) = &doc.file_name {
            for ln in &languages {
                if ln.extensions.is_match(&name) {
                    l = ln.clone();
                    break;
                }
            }
        }
        let mut buffer = Buffer::new(doc, l);
        buffer.status = Some(Status::Message(HELP.to_string(), Instant::now()));
        Editor {
            buffer,
            ui,
            languages,
        }
    }

    fn open_file(&mut self, name: &str) {
        let mut l = lang::text();
        for ln in &self.languages {
            if ln.extensions.is_match(name) {
                l = ln.clone();
                break;
            }
        }
        match Document::open(name) {
            Err(e) => {
                self.buffer.status = Some(e.into());
            }
            Ok(doc) => {
                self.buffer = Buffer::new(doc, l);
            }
        }
    }

    pub fn run(&mut self) -> io::Result<()> {
        let (_, height) = self.ui.bounds();

        loop {
            self.ui.render(&self.buffer, &self.buffer.cursor)?;
            match self.ui.read_input()? {
                key!(Key::Ctrl('o')) => {
                    // TODO: don't just discard buffers like this
                    if let Some(file_name) = self.prompt("file", |_, _, _| {})? {
                        self.open_file(&file_name);
                    }
                }
                key!(Key::Ctrl('q')) => {
                    if self.buffer.is_dirty() {
                        loop {
                            if let Some(response) =
                                self.prompt("quit without saving? [Y/n]", |_, _, _| {})?
                            {
                                match response.as_ref() {
                                    "Y" => return Ok(()),
                                    "n" => break,
                                    _ => {}
                                }
                            } else {
                                // Happens with CTRL-g
                                break;
                            }
                        }
                    } else {
                        return Ok(());
                    }
                }
                key!(Key::Ctrl('a')) => {
                    self.buffer.move_cursor(Movement::Start);
                }
                key!(Key::Ctrl('e')) => {
                    self.buffer.move_cursor(Movement::End);
                }
                key!(Key::Ctrl('f')) => {
                    self.buffer
                        .move_cursor(Movement::ScrollDown(height.saturating_sub(2)));
                }
                key!(Key::Ctrl('b')) => {
                    self.buffer
                        .move_cursor(Movement::ScrollUp(height.saturating_sub(2)));
                }
                key!(Key::Ctrl('s')) => {
                    self.search()?;
                }
                key!(Key::Up) | mouse_press!(MouseButton::WheelUp) => {
                    self.buffer.move_cursor(Movement::Up);
                }
                key!(Key::Left) => {
                    self.buffer.move_cursor(Movement::Left);
                }
                key!(Key::Down) | mouse_press!(MouseButton::WheelDown) => {
                    self.buffer.move_cursor(Movement::Down);
                }
                key!(Key::Right) => {
                    self.buffer.move_cursor(Movement::Right);
                }
                key!(Key::Delete) => {
                    self.buffer.delete();
                }
                key!(Key::Backspace) => {
                    self.buffer.move_cursor(Movement::Left);
                    self.buffer.delete();
                }
                key!(Key::Char(c)) => {
                    let inserted = self.buffer.insert(c);
                    for _ in 0..inserted {
                        self.buffer.move_cursor(Movement::Right);
                    }
                }
                key!(Key::Ctrl('w')) => {
                    self.save()?;
                }
                _ => {}
            }
            self.ui.scroll(&self.buffer.cursor);
        }
    }

    fn save(&mut self) -> io::Result<()> {
        let res = if self.buffer.document.is_ephemeral() {
            if let Some(file_name) = self.prompt("save as", |_, _, _| {})? {
                self.buffer.document.save_as(file_name)
            } else {
                return Ok(());
            }
        } else {
            self.buffer.document.save()
        };
        if let Err(e) = res {
            self.buffer.status = Some(Status::from(e));
        } else if let Some(name) = self.buffer.document.file_name.as_ref() {
            self.buffer.status = Some(Status::Message(
                format!("saved to {}", name),
                Instant::now(),
            ));
        }
        Ok(())
    }

    fn search(&mut self) -> io::Result<()> {
        let orig_cursor = self.buffer.cursor.clone();
        let mut search = Search::Forward;
        let cb = |editor: &mut Self, ev: Event, query: &String| {
            let mut moved = false;
            match ev {
                key!(Key::Ctrl('n')) => {
                    editor.buffer.move_cursor(Movement::Right);
                    moved = true;
                    search = Search::Forward;
                }
                // Previous match
                key!(Key::Ctrl('p')) => {
                    editor.buffer.move_cursor(Movement::Left);
                    moved = true;
                    search = Search::Backward;
                }
                _ => {}
            }
            if let Some(pos) = search.apply(&editor.buffer, query) {
                editor.buffer.set_cursor(pos);
                editor.ui.scroll(&pos);
            } else if moved {
                match search {
                    Search::Forward => editor.buffer.move_cursor(Movement::Left),
                    Search::Backward => editor.buffer.move_cursor(Movement::Right),
                }
            }
        };
        if let Some(needle) = self.prompt("search for", cb)? {
            if let Some(pos) = search.apply(&self.buffer, &needle) {
                self.buffer.set_cursor(pos);
                self.ui.scroll(&pos);
            } else {
                self.buffer.set_cursor(orig_cursor);
                self.buffer.status = Some(Status::Message("Not found".to_owned(), Instant::now()));
            }
        } else {
            // cancelled
            self.buffer.set_cursor(orig_cursor);
        };
        Ok(())
    }

    fn prompt<C>(&mut self, msg: &str, mut callback: C) -> io::Result<Option<String>>
    where
        C: FnMut(&mut Self, Event, &String),
    {
        // TODO can I abstract out this logic into a proper modal editor?
        let mut result = String::new();
        let (width, _) = self.ui.bounds();
        let min_x = msg.len() + 2;
        let mut x = min_x;

        loop {
            // Render a fake cursor
            let mut with_cursor = result.clone();
            with_cursor.insert_grapheme(x, CURSOR_BLOCK);

            self.buffer.status = Some(Status::Prompt(format!("{}: {}", msg, with_cursor)));
            self.ui.render(&self.buffer, &self.buffer.cursor)?;
            let event = self.ui.read_input()?;
            match event {
                key!(Key::Ctrl('g')) => {
                    self.buffer.status = None;
                    return Ok(None);
                }
                key!(Key::Ctrl('a')) => {
                    x = min_x;
                }
                key!(Key::Ctrl('e')) => {
                    x = width;
                }
                key!(Key::Left) => {
                    if x > min_x {
                        x -= 1;
                    }
                }
                key!(Key::Right) => {
                    if x < width {
                        x += 1;
                    }
                }
                key!(Key::Delete) => {
                    // todo delete
                    result.remove_grapheme(x.saturating_sub(min_x));
                }
                key!(Key::Backspace) => {
                    if x > min_x {
                        x -= 1;
                        result.remove_grapheme(x.saturating_sub(min_x));
                    }
                }
                key!(Key::Char('\n')) => break,
                key!(Key::Char(c)) => {
                    result.push(c);
                    // TODO: scroll the contents left?
                    x += 1;
                }
                _ => {}
            }
            // Allow something to handle this
            callback(self, event, &result);
        }
        self.buffer.status = None;
        Ok(Some(result))
    }
}
