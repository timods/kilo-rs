{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = with pkgs;[
   cargo
   cargo-flamegraph
   rust-analyzer
   (rust-nightly."2021-02-03".withComponents [
         "cargo"
         "clippy-preview"
         "rust-src"
         "rust-std"
         "rustc"
         "rustfmt-preview"
       ])
  ];
}
