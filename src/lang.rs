use regex::Regex;

pub enum Item {
    Keyword,
    Type,
    String,
    Literal,
    Numeric,
    Special,
    Comment,
}

pub trait Annotator {
    fn annotate(&self, s: &str) -> Option<Item>;
}

#[derive(Debug, Clone)]
pub struct Language {
    pub name: String,
    pub extensions: Regex,
    // TODO use a regex and cache this stuff
    keywords: Option<Regex>,
    types: Option<Regex>,
    comments: Option<Regex>,
}

impl Annotator for &Language {
    // Brain-dead styling. This needs a way to indicate "style _until_ some criteria..."
    fn annotate(&self, s: &str) -> Option<Item> {
        if let Some(keywords) = &self.keywords {
            if keywords.is_match(s) {
                Some(Item::Keyword)
            } else {
                None
            }
        } else if let Some(types) = &self.types {
            if types.is_match(s) {
                Some(Item::Type)
            } else {
                None
            }
        } else if let Some(comments) = &self.comments {
            if comments.is_match(s) {
                Some(Item::Comment)
            } else {
                None
            }
        } else {
            None
        }
    }
}

pub fn text() -> Language {
    Language {
        name: "text".into(),
        extensions: Regex::new(r"\.txt$").unwrap(),
        keywords: None,
        types: None,
        comments: None,
    }
}

pub fn known() -> Vec<Language> {
    vec![
        Language {
            name: "rust".into(),
            extensions: Regex::new(r"\.rs$").unwrap(),
            keywords: Some(Regex::new(r"\b(impl|fn|pub|use|struct|let|mut|mod|match|enum|const|if|while|loop|for|type|in|continue|break|true|false|self|dyn|where|else)\b").unwrap()),
            types: Some(Regex::new(r"\b(String|u8|u16|u32|u64|i8|i16|i32|i64|str|Vec|Option|Result|char|Instant|Some|None|Err|Ok|usize|isize)\b").unwrap()),
            comments: Some(Regex::new(r"//").unwrap()),
        },
    ]
}
