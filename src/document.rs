use snafu::{ResultExt, Snafu};
use std::cmp::PartialEq;
use std::convert::AsRef;
use std::fs::{File, OpenOptions};
use std::io::{self, BufRead, BufReader, Seek, SeekFrom, Write};
use std::path::{Path, PathBuf};

use crate::line::Line;
use crate::position::Position;

#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("failed to open {}: {}", path.display(), source))]
    Open { source: io::Error, path: PathBuf },
    #[snafu(display("failed to read contents of {}: {}", path.display(), source))]
    Read { source: io::Error, path: PathBuf },
    #[snafu(display("failed to save buffer: {}", source))]
    Save { source: io::Error },
}

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug)]
pub struct Document {
    pub file_name: Option<String>,
    file: Option<File>,
    lines: Vec<Line>,
    pub dirty: bool,
}

impl Document {
    pub fn in_memory(lines: Vec<Line>) -> Self {
        Document {
            file_name: None,
            file: None,
            lines,
            dirty: false,
        }
    }

    pub fn empty() -> Self {
        Document {
            file_name: None,
            file: None,
            lines: Vec::new(),
            dirty: false,
        }
    }

    pub fn open<P: AsRef<Path>>(p: P) -> Result<Self> {
        let path = p.as_ref();
        let file = OpenOptions::new()
            .read(true)
            .write(true)
            .create(true)
            .open(p.as_ref())
            .context(Open { path })?;
        let mut rdr = BufReader::new(file);
        let mut lines = Vec::new();
        loop {
            let mut line = String::new();
            let n = rdr.read_line(&mut line).context(Read { path })?;
            if n == 0 {
                break;
            }
            // trim CRLF
            if line.ends_with('\n') {
                line.pop();
                if line.ends_with('\r') {
                    line.pop();
                }
            }
            lines.push(line.into());
        }
        let file_name = Some(path.to_string_lossy().to_string());
        let file = Some(rdr.into_inner());
        Ok(Document {
            file_name,
            file,
            lines,
            dirty: false,
        })
    }

    /// A document is ephemeral if it has no backing file
    pub fn is_ephemeral(&self) -> bool {
        return self.file.is_none();
    }

    pub fn save(&mut self) -> Result<()> {
        if self.file.is_none() || !self.dirty {
            return Ok(());
        }
        let mut file = self.file.take().unwrap();
        file.seek(SeekFrom::Start(0)).context(Save)?;

        for line in self.lines.iter() {
            file.write_all(line.as_bytes()).context(Save)?;
            file.write(b"\n").context(Save)?;
        }
        file.flush().context(Save)?;
        self.file = Some(file);
        self.dirty = false;
        Ok(())
    }

    pub fn save_as<P: AsRef<Path>>(&mut self, p: P) -> Result<()> {
        let path = p.as_ref();
        self.file = Some(
            OpenOptions::new()
                .read(true)
                .write(true)
                .create(true)
                .open(p.as_ref())
                .context(Open { path })?,
        );
        self.file_name = Some(path.to_string_lossy().to_string());
        self.dirty = true;
        self.save()
    }

    /// Insert a unicode grapheme cluster at the specified position,
    pub fn insert(&mut self, at: &Position, ch: char) -> usize {
        self.dirty = true;
        if ch == '\n' {
            return self.insert_newline(at);
        }
        if at.y == self.len() {
            let mut line = Line::default();
            line.insert(0, ch);
            self.lines.push(line);
        } else if at.y < self.len() {
            let line = self.lines.get_mut(at.y).unwrap();
            line.insert(at.x, ch)
        }
        // The number of characters inserted
        if ch == '\t' {
            2
        } else {
            1
        }
    }

    fn insert_newline(&mut self, at: &Position) -> usize {
        if at.y > self.len() {
            return 0;
        }
        // Get indentation depth
        if at.y == self.len() {
            let indentation = self.lines[at.y - 1].indentation();
            self.lines.push(Line::from(" ".repeat(indentation)));
            indentation + 1
        } else {
            let indentation = self.lines[at.y].indentation();
            let mut new_line = self.lines.get_mut(at.y).unwrap().split(at.x);
            if indentation > 0 {
                new_line = Line::from(format!("{}{}", " ".repeat(indentation), new_line));
            }
            self.lines.insert(at.y + 1, new_line);
            indentation + 1
        }
    }

    /// Delete the grapheme at the specified position.
    pub fn delete(&mut self, at: &Position) {
        let len = self.len();
        if at.y >= self.len() {
            return;
        }
        self.dirty = true;
        if at.x == self.line(at.y).unwrap().len() && at.y < len - 1 {
            let next_line = self.lines.remove(at.y + 1);
            let line = self.lines.get_mut(at.y).unwrap();
            line.append(next_line);
        } else {
            let line = self.lines.get_mut(at.y).unwrap();
            line.delete(at.x);
        }
    }

    /// Find the first occurrence of the provided string in this document
    /// TODO regex w/ spaces converted to `.` like in emacs
    pub fn find(&self, needle: &str, after: &Position) -> Option<Position> {
        for (y, line) in self.lines.iter().enumerate().skip(after.y) {
            // TODO implement x skipping? This is harder
            let mut after_x = 0;
            if y == after.y {
                after_x = after.x;
            }
            if let Some(x) = line.find(needle, after_x) {
                return Some(Position { x, y });
            }
        }
        None
    }

    pub fn rfind(&self, needle: &str, before: &Position) -> Option<Position> {
        for (y, line) in self.lines.iter().enumerate().take(before.y + 1).rev() {
            // TODO implement x skipping? This is harder
            let mut before_x = line.len();
            if y == before.y {
                before_x = before.x;
            }
            if let Some(x) = line.rfind(needle, before_x) {
                return Some(Position { x, y });
            }
        }
        None
    }

    pub fn line(&self, index: usize) -> Option<&Line> {
        self.lines.get(index)
    }

    pub fn line_len(&self, index: usize) -> usize {
        self.lines.get(index).map_or(0, |l| l.len())
    }

    pub fn len(&self) -> usize {
        self.lines.len()
    }

    pub fn size(&self) -> usize {
        self.lines.iter().map(|l| l.len() + 2).sum::<usize>() // crlf for each line
    }
}

impl PartialEq for Document {
    fn eq(&self, other: &Self) -> bool {
        self.lines == other.lines && self.file_name == other.file_name
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::line::Line;
    use k9::assert_equal;

    #[test]
    fn document_insert_when_empty() {
        let mut doc = Document::empty();
        doc.insert(&Position { x: 0, y: 0 }, 'c');
        let mline = doc.line(0);
        assert_equal!(mline.is_some(), true);
        let line = mline.unwrap();
        assert_equal!(line, &Line::from("c"));
    }

    #[test]
    fn document_insert_active() {
        let mut doc = Document::in_memory(vec![
            Line::from("first line"),
            Line::from("second line"),
            Line::from("third line"),
        ]);

        doc.insert(&Position { x: 6, y: 1 }, '!');

        assert_equal!(
            doc,
            Document::in_memory(vec![
                Line::from("first line"),
                Line::from("second! line"),
                Line::from("third line"),
            ])
        );
    }

    #[test]
    fn document_delete_combines_lines() {
        let mut doc = Document::in_memory(vec![Line::from("a"), Line::from("b")]);

        doc.delete(&Position { x: 1, y: 0 });

        assert_equal!(doc, Document::in_memory(vec![Line::from("ab"),]));
    }

    #[test]
    fn document_append_newline() {
        let mut doc = Document::in_memory(vec![Line::from("a")]);

        doc.insert_newline(&Position { x: 0, y: 1 });

        assert_equal!(
            doc,
            Document::in_memory(vec![Line::from("a"), Line::default()])
        );
    }

    #[test]
    fn document_insert_newline_splits() {
        let mut doc = Document::in_memory(vec![Line::from("ab")]);

        doc.insert_newline(&Position { x: 1, y: 0 });

        assert_equal!(
            doc,
            Document::in_memory(vec![Line::from("a"), Line::from("b")])
        );
    }

    #[test]
    fn document_find() {
        let doc = Document::in_memory(vec![Line::from("first line"), Line::from("second line")]);

        assert_equal!(
            doc.find(&"line", &Position::default()),
            Some(Position { x: 6, y: 0 })
        );
    }

    #[test]
    fn document_find_after() {
        let doc = Document::in_memory(vec![Line::from("first line"), Line::from("second line")]);

        assert_equal!(
            doc.find(&"line", &Position { x: 7, y: 0 }),
            Some(Position { x: 7, y: 1 })
        );
    }

    #[test]
    fn document_find_after_inline() {
        let doc = Document::in_memory(vec![Line::from("first line second line")]);

        assert_equal!(
            doc.find(&"line", &Position { x: 7, y: 0 }),
            Some(Position { x: 18, y: 0 })
        );
    }

    #[test]
    fn document_rfind() {
        let doc = Document::in_memory(vec![Line::from("first line"), Line::from("second line")]);

        assert_equal!(
            doc.rfind(&"line", &Position { x: 11, y: 2 }),
            Some(Position { x: 7, y: 1 })
        );
    }

    #[test]
    fn document_rfind_before() {
        let doc = Document::in_memory(vec![Line::from("first line"), Line::from("second line")]);

        assert_equal!(
            doc.rfind(&"line", &Position { x: 7, y: 1 }),
            Some(Position { x: 6, y: 0 })
        );
    }

    #[test]
    fn document_rfind_before_inline() {
        let doc = Document::in_memory(vec![Line::from("first line second line")]);

        assert_equal!(
            doc.rfind(&"line", &Position { x: 18, y: 0 }),
            Some(Position { x: 6, y: 0 })
        );
    }
}
